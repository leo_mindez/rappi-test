package mx.leo.testrappi.listing.adapter

import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.theme_listing_item.view.*
import mx.leo.easyrecycler.viewholder.EasyViewHolder
import mx.leo.testrappi.listing.model.Data

/**
 * Created by Leo on 13/05/17.
 */
class ThemeItemViewHolder(view: View?) : EasyViewHolder(view) {

    fun bindItem(data: Data){
        Log.d("image",data.header_image +"")
        itemView.theme_title.setText(data.title)
        itemView.theme_cover_img.setImageURI(if(!data.banner_image.equals("")) data.banner_image else data.icon_img)
    }
}