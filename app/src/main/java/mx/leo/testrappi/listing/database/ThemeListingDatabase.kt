package mx.leo.testrappi.listing.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.SyncStateContract
import android.util.Log
import com.vansuita.sqliteparser.SqlParser
import mx.leo.testrappi.listing.model.Data
import mx.leo.testrappi.listing.model.Theme
import java.util.concurrent.Callable
import java.util.concurrent.FutureTask

/**
 * Created by leomendez on 14/05/17.
 */
class ThemeListingDatabase(context: Context?) : SQLiteOpenHelper(context, Contract.TABLE_NAME, null, Contract.TABLE_VERSION) {

    private val SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Contract.TABLE_NAME

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SqlParser.create(Contract.TABLE_NAME)
                .pk(Contract.ID)
                .str(Contract.COLUMN_THEME_DISPLAY_NAME)
                .str(Contract.COLUMN_THEME_BANNER_IMAGE)
                .str(Contract.COLUMN_THEME_HEADER_IMAGE)
                .str(Contract.COLUMN_THEME_TITLE)
                .str(Contract.COLUMN_THEME_DESCRIPTION)
                .str(Contract.COLUMN_THEME_IMAGE)
                .str(Contract.COLUMN_THEME_ICON_IMG).build())
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    fun writeAll(content:ArrayList<ContentValues>){
        val db = writableDatabase
       content.forEach { contentValue -> db.insert(Contract.TABLE_NAME,null,contentValue) }
    }

    fun asyncWriteAll(content:ArrayList<Theme>){
        val future = FutureTask<Unit>({
            val db = writableDatabase

            content.forEach {  theme ->
            val contentValue = ContentValues()
            contentValue.put(Contract.COLUMN_THEME_DISPLAY_NAME,theme.data.display_name)
            contentValue.put(Contract.COLUMN_THEME_BANNER_IMAGE,theme.data.banner_image)
            contentValue.put(Contract.COLUMN_THEME_HEADER_IMAGE,theme.data.header_image)
            contentValue.put(Contract.COLUMN_THEME_TITLE,theme.data.title)
            contentValue.put(Contract.COLUMN_THEME_DESCRIPTION,theme.data.description)
            contentValue.put(Contract.COLUMN_THEME_ICON_IMG,theme.data.icon_img)

                db.insert(Contract.TABLE_NAME,null,contentValue) }
            db.close()
        })
        future.run()
    }

    fun asyncGetAll(callback:(response:ArrayList<Theme>) -> Unit){

        val db = readableDatabase

        val future = FutureTask<ArrayList<Theme>>(Callable<ArrayList<Theme>> {

            val values = ArrayList<Theme>()
            val projection = arrayOf<String>(Contract.ID,
                    Contract.COLUMN_THEME_DISPLAY_NAME,
                    Contract.COLUMN_THEME_BANNER_IMAGE,
                    Contract.COLUMN_THEME_HEADER_IMAGE,
                    Contract.COLUMN_THEME_TITLE,
                    Contract.COLUMN_THEME_DESCRIPTION,
                    Contract.COLUMN_THEME_IMAGE,
                    Contract.COLUMN_THEME_ICON_IMG)

            val cursor = db.query(Contract.TABLE_NAME,projection,null,null,null,null,null)
            cursor.moveToFirst()

            while (cursor.moveToNext()){
                values.add(Theme(Data(cursor.getString(cursor.getColumnIndex(Contract.COLUMN_THEME_DISPLAY_NAME))
                        ,cursor.getString(cursor.getColumnIndex(Contract.COLUMN_THEME_BANNER_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(Contract.COLUMN_THEME_HEADER_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(Contract.COLUMN_THEME_TITLE)),
                        cursor.getString(cursor.getColumnIndex(Contract.COLUMN_THEME_DESCRIPTION)),
                        cursor.getString(cursor.getColumnIndex(Contract.COLUMN_THEME_ICON_IMG))
                )))
            }

            cursor.close()
            values
        })

        future.run()
        if(future.isDone){
            db.close()
            callback(future.get())
        }
    }
}