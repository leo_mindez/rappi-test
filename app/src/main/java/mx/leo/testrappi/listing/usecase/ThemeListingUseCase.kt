package mx.leo.testrappi.listing.usecase

import mx.leo.testrappi.base.data.net.NetManager
import mx.leo.testrappi.base.usecase.UseCase
import mx.leo.testrappi.listing.model.ThemeResponse

/**
 * Created by Leo on 13/05/17.
 */

class ThemeListingUseCase(var netManager: NetManager):UseCase<Void,ThemeResponse>() {
    override fun execute(params: Void?, success: (ThemeResponse) -> Unit, error: (Error) -> Unit) {
        netManager.get<ThemeResponse>("https://www.reddit.com/reddits.json",success = {response ->
            success(response)
        },error = {
            error -> error(error)
        })
    }
}

