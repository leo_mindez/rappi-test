package mx.leo.testrappi.listing.usecase

import android.database.sqlite.SQLiteOpenHelper
import mx.leo.testrappi.listing.database.ThemeListingDatabase
import mx.leo.testrappi.listing.model.Theme

/**
 * Created by leomendez on 14/05/17.
 */
class ThemeListingOfflineUseCase(var database:ThemeListingDatabase) {

    fun saveThemeList(themeListing:ArrayList<Theme>){
        database.asyncWriteAll(themeListing)
    }

    fun getThemeList(callback:(ArrayList<Theme>) -> Unit){
        database.asyncGetAll(callback)
    }
}