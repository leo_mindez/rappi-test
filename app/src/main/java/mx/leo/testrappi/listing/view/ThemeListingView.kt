package mx.leo.testrappi.listing.view

import mx.leo.testrappi.base.view.LoadingView
import mx.leo.testrappi.listing.model.Data
import mx.leo.testrappi.listing.model.Theme

/**
 * Created by Leo on 13/05/17.
 */
interface ThemeListingView : LoadingView{
    fun showThemeListing(data:ArrayList<Theme>)
}