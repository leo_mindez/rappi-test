package mx.leo.testrappi.listing.database

import android.provider.BaseColumns

/**
 * Created by leomendez on 14/05/17.
 */
class Contract{
    companion object {
        const val TABLE_NAME = "themes"
        const val ID = "_id"
        const val TABLE_VERSION = 1
        const val COLUMN_THEME_DISPLAY_NAME = "display_name"
        const val COLUMN_THEME_BANNER_IMAGE = "banner_image"
        const val COLUMN_THEME_HEADER_IMAGE = "header_image"
        const val COLUMN_THEME_TITLE = "title"
        const val COLUMN_THEME_DESCRIPTION = "description"
        const val COLUMN_THEME_IMAGE = "image"
        const val COLUMN_THEME_ICON_IMG = "icon_image"
    }
}