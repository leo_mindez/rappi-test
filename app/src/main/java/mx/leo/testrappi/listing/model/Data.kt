package mx.leo.testrappi.listing.model

/**
 * Created by Leo on 13/05/17.
 */
data class Data(var display_name:String? = "", var banner_image:String?="",var header_image:String?="", var title:String?="", var description:String?="", var icon_img:String?="")