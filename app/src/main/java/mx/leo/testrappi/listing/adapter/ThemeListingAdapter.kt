package mx.leo.testrappi.listing.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import mx.leo.easyrecycler.adapter.EasyAdapter
import mx.leo.testrappi.R
import mx.leo.testrappi.listing.model.Data
import mx.leo.testrappi.listing.model.Theme

/**
 * Created by Leo on 13/05/17.
 */
class ThemeListingAdapter: EasyAdapter<ThemeItemViewHolder, Theme>() {

    override fun createHolder(parent: ViewGroup?, viewType: Int): ThemeItemViewHolder  =
            ThemeItemViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.theme_listing_item,parent,false))

    override fun onBindItemViewHolder(holder: ThemeItemViewHolder, item: Theme, position: Int) {
        holder.bindItem(item.data)
    }
}