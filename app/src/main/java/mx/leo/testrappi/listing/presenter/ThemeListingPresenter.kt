package mx.leo.testrappi.listing.presenter

import mx.leo.testrappi.base.presenter.Presenter
import mx.leo.testrappi.listing.usecase.ThemeListingOfflineUseCase
import mx.leo.testrappi.listing.usecase.ThemeListingUseCase
import mx.leo.testrappi.listing.view.ThemeListingView

/**
 * Created by Leo on 13/05/17.
 */
class ThemeListingPresenter(var useCase: ThemeListingUseCase, var offlineUseCase: ThemeListingOfflineUseCase):Presenter<ThemeListingView>() {

    fun showThemes(){
        view?.showLoadingView()
        useCase.execute(success = {
            response ->
            view?.hideLoadingView()
            view?.showThemeListing(response.data.children)
            offlineUseCase.saveThemeList(response.data.children)
        },error = {
            error ->
            view?.hideLoadingView()
            view?.showErrorMessage(error)
        })
    }

    fun getOfflineThemes(){
        view?.showLoadingView()
        offlineUseCase.getThemeList {
            themes ->
            view?.hideLoadingView()
            view?.showThemeListing(themes)
        }
    }
}