package mx.leo.testrappi.listing.view.activity

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import kotlinx.android.synthetic.main.activity_listing.*

import mx.leo.testrappi.R
import mx.leo.testrappi.base.data.net.NetManager
import mx.leo.testrappi.listing.adapter.ThemeListingAdapter
import mx.leo.testrappi.listing.database.ThemeListingDatabase
import mx.leo.testrappi.listing.model.Theme
import mx.leo.testrappi.listing.presenter.ThemeListingPresenter
import mx.leo.testrappi.listing.usecase.ThemeListingOfflineUseCase
import mx.leo.testrappi.listing.usecase.ThemeListingUseCase
import mx.leo.testrappi.listing.view.ThemeListingView
import java.util.ArrayList
import android.net.ConnectivityManager
import android.view.View
import mx.leo.easyrecycler.util.RecyclerViewItemClickListener
import mx.leo.easyrecycler.util.extensions.onItemClickListener
import mx.leo.testrappi.detail.view.DetailActivity


class ListingActivity : AppCompatActivity(),ThemeListingView{

    lateinit var themeAdapter:ThemeListingAdapter
    private var dialogProgress : ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listing)
        themeAdapter = ThemeListingAdapter()
        getThemeListing()
        configRecyclerView()
        dialogProgress = ProgressDialog(this)
        dialogProgress?.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
        dialogProgress?.setMessage(getString(R.string.theme_listing_progress_msg))


    }

    override fun showErrorMessage(error: Error) {
        Log.e("Themes Error",error.message)
    }

    override fun showLoadingView() {
        dialogProgress?.show()
    }

    override fun hideLoadingView() {
        dialogProgress?.hide()
    }

    override fun showThemeListing(data: ArrayList<Theme>) {
        themeAdapter.addItems(data)
    }

    private fun getThemeListing(){

        val usecase = ThemeListingUseCase(NetManager())
        val offlineUseCase = ThemeListingOfflineUseCase(ThemeListingDatabase(this))
        val presenter = ThemeListingPresenter(usecase,offlineUseCase)
        presenter.view = this

        if(isThereInternet()){
            presenter.showThemes()
        }else{
            presenter.getOfflineThemes()
        }
    }

    private fun configRecyclerView(){
        theme_listing.layoutManager = LinearLayoutManager(this)
        theme_listing.adapter = themeAdapter
        theme_listing.setHasFixedSize(true)
        theme_listing.onItemClickListener(object :RecyclerViewItemClickListener.OnItemClickListener {
            override fun onItemClick(view: View?, position: Int?) {
                val detailActivity = Intent(baseContext,DetailActivity::class.java)
                detailActivity.putExtra("theme_title",themeAdapter.items[position!!].data.title)
                detailActivity.putExtra("theme_description",themeAdapter.items[position].data.description)
                startActivity(detailActivity)
            }

        })
    }


    private fun isThereInternet(): Boolean {
       val connectionManager =  (this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        return if(connectionManager.activeNetworkInfo != null) connectionManager.activeNetworkInfo.isConnectedOrConnecting else false
    }

}
