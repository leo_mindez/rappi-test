package mx.leo.testrappi.listing.model

/**
 * Created by Leo on 13/05/17.
 */
data class ThemeData(var children:ArrayList<Theme>)