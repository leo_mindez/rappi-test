package mx.leo.testrappi.detail.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import mx.leo.testrappi.R
import kotlinx.android.synthetic.main.activity_detail.*


class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        showDetails(intent?.extras?.getString("theme_title"),intent?.extras?.getString("theme_description"))
    }

    private fun showDetails(title:String?, description:String?){
        theme_title.setText(title)
        theme_description.setText(description)
    }
}

