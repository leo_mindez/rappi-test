package mx.leo.testrappi.application

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

/**
 * Created by Leo on 13/05/17.
 */
class App:Application() {

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
    }
}