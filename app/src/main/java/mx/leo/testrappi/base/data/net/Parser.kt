package mx.leo.testrappi.base.data.net

import com.google.gson.Gson

/**
 * Class to parse JSON Object by using GSON Library
 */
inline fun <reified T> parse(content: String): T = Gson().fromJson(content, T::class.java)

