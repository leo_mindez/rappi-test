package mx.leo.testrappi.base.presenter

import mx.leo.testrappi.base.view.View

/**
 * Abstract class to handle business logic
 */

abstract class Presenter<V: View>(){
    var view:V? = null;
}
